mkdir ./files
mkdir ./files/presentations
mkdir ./files/pdfs
mkdir ./files/images
mkdir ./files/text
mkdir ./files/spreadsheets
mkdir ./files/sounds
mkdir ./files/videos
mkdir ./files/setups
mkdir ./files/archives
mkdir ./files/disk_images
mkdir ./files/torrents
mkdir ./files/other
mkdir ./files/folders
mv -v ./*.ppt ./files/presentations
mv -v ./*.pptx ./files/presentations
mv -v ./*.pdf ./files/pdfs
mv -v ./*.png ./files/images
mv -v ./*.img ./files/images
mv -v ./*.jpg ./files/images
mv -v ./*.jpeg ./files/images
mv -v ./*.bmp ./files/images
mv -v ./*.doc ./files/text
mv -v ./*.docx ./files/text
mv -v ./*.odt ./files/text
mv -v ./*.txt ./files/text
mv -v ./*.xls ./files/spreadsheets
mv -v ./*.xlsx ./files/spreadsheets
mv -v ./*.mp3 ./files/sounds
mv -v ./*.wav ./files/sounds
mv -v ./*.flac ./files/sounds
mv -v ./*.avi ./files/videos
mv -v ./*.wmv ./files/videos
mv -v ./*.mp4 ./files/videos
mv -v ./*.exe ./files/setups
mv -v ./*.apk ./files/setups
mv -v ./*.msi ./files/setups
mv -v ./*.deb ./files/setups
mv -v ./*.rar ./files/archives
mv -v ./*.7z ./files/archives
mv -v ./*.7za ./files/archives
mv -v ./*.7zb ./files/archives
mv -v ./*.zip ./files/archives
mv -v ./*.tar ./files/archives
mv -v ./*.mxf ./files/disk_images
mv -v ./*.mdf ./files/disk_images
mv -v ./*.mda ./files/disk_images
mv -v ./*.iso ./files/disk_images
mv -v ./*.torrent ./files/torrents
mv -v ./*.* ./files/other
#mv -v ./* ./files/folders
mv ./files/other/cleaner.ps1 ./
mv ./files/other/start.bat ./
cmd /c pause