# README #

This is a really simple script for cleaning up folders based on file extensions.
Feel free to commit more extensions.

### Script ###

This script sorts all your different files in a directory to ./files/...
It creates separate directories inside ./files for "each" type of file.

### Usage ###

Just download/clone it. There's not much more to it. Every version has an additional readme file.

### Current extensions supported ###

* Presentations
* *.ppt 
* *.pptx
* PDFs
* *.pdf 
* Images
* *.png 
* *.img 
* *.jpg 
* *.jpeg
* *.bmp 
* Text
* *.doc 
* *.docx
* *.odt 
* *.txt 
* Spreadsheets
* *.xls 
* *.xlsx
* Sounds
* *.mp3 
* *.wav 
* *.flac
* *.avi 
* *.wmv 
* *.mp4 
* Executables
* *.exe 
* *.apk 
* *.msi 
* *.deb 
* Archives
* *.rar 
* *.7z
* *.7za 
* *.7zb 
* *.zip 
* *.tar 
* Disk Images
* *.mxf 
* *.mdf 
* *.mda 
* *.iso 
* Torrents
* *.torrent

* Rest of the files just go to ./files/other
* Folders go to ./files/folders

### Author ###

Me. Just me.